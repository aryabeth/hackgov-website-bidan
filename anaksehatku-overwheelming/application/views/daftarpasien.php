<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Daftar Pasien</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,300,600,700' rel='stylesheet' type='text/css'>

    <!-- Revolution css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo base_url(); ?>assets/vendor/rs-plugin/css/extralayer.css">

    <!-- Flat icon css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/flat-icon/flaticon.css">

    <!-- Font awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css">

    <!-- Owl Carosel css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl/css/owl.theme.default.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl/css/owl.theme.css">

    <!-- mmenu -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/mmenu/css/jquery.mmenu.css" />

    <!-- Bootstrap css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.css">

    <!-- Animate css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">

    <!-- Custom Style css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/hover.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">


</head><!--/head-->

<body id="home" class="main">
    <header class="header-part">
        <div id="home" class="wrapper">
            <!-- Fixed navbar -->
            <div class="navi navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header page-scroll">
                        <a href="#menu">
                            <button type="button" data-effect="st-effect-1" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </a>
                    <a class="navbar-brand" href="index.html"><img src="<?php echo base_url(); ?>assets/img/logo2.png" alt="AnakSehatku"></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse pull-right hidden-xs">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="page-scroll" href="<?php echo base_url(); ?>welcome#home">Home</a></li>
                                <li> <a class="page-scroll" href="<?php echo base_url(); ?>welcome#about">about us</a></li>
                                <li> <a class="page-scroll" href="<?php echo base_url(); ?>welcome#services">imunisasi</a></li>
                                <li> <a class="page-scroll" href="<?php echo base_url(); ?>welcome#product">Photo</a></li>
                                <li> <a class="page-scroll" href="<?php echo base_url(); ?>welcome#blog">info</a></li>
                                <li><a href="<?php echo base_url(); ?>login">Login</a></li>
                            <!-- Search Block -->
                            <li>
                                <i class="search fa fa-search search-btn"></i>
                                <div class="search-open" style="display:none;">
                                    <div class="input-group animated fadeInDown">
                                        <input type="text" class="form-control" placeholder="Search">
                                        <span class="input-group-btn">
                                            <button class="btn-u" type="button">Go</button>
                                        </span>
                                    </div>
                                </div>
                            </li>
                            <!-- End Search Block -->
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div><!-- End of Nav -->
        </div>
    </header>

    <section id="daftar" class="contact-wrapper section-padding">
        <div class="container">
            <div class="row">
                <div class="wow zoomIn col-xs-12 text-center p-padding">
                    <h1 class="section-title">daftar pasien</h1>
                    <p>Isikan form berikut ini dengan data yang benar menurut kartu identitas anda</p>
                </div><!-- col-xs-12 -->
                <div class="wow zoomIn col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
                    <form name="contactForm" id='contact_form' method="post" action="<?php echo base_url(); ?>daftar/daftarpasien">
                        <div class="form-inline">
                            <div class="form-group col-sm-9">
                                <input type="text" class="form-control" name="namaanak" id="namaanak" placeholder="nama anak">
                            </div>
                            <div class="form-group col-sm-3">
                                <select class="form-control" name="jeniskelamin" id="jeniskelamin" required style="border-radius:0px">
                                    <option value="" selected>- Jenis Kelamin -</option>
                                    <option value="laki-laki">laki-laki</option>
                                    <option value="perempuan">perempuan</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-4">
                                <input type="text" class="form-control" name="tempatanak" id="tempatanak" placeholder="tempat lahir anak">
                            </div>
                            <div class="form-group col-sm-4" id="sandbox-container">
                                <input type="text" class="form-control" name="tanggalanak" id="tanggalanak" data-provide="datepicker" placeholder="tanggal lahir anak">
                            </div>

                            <div class="form-group col-sm-4">
                                <input type="text" class="form-control" name="umur" id="umur" placeholder="umur">
                            </div>

                            <div class="form-group col-sm-12">
                                <hr>
                            </div>

                            <div class="form-group col-sm-6">
                                <input type="text" class="form-control" name="namaibu" id="namaibu" placeholder="nama ibu">
                            </div>
                            <div class="form-group col-sm-6">
                                <input type="text" class="form-control" name="suami" id="suami" placeholder="nama suami">
                            </div>
                            <div class="form-group col-sm-3">
                                <input type="text" class="form-control" name="tempatibu" id="tempatibu" placeholder="tempat lahir ibu">
                            </div>
                            <div class="form-group col-sm-3">
                                <input type="text" class="form-control" name="tanggalibu" id="tanggalibu" placeholder="tanggal lahir ibu">
                            </div>
                            <div class="form-group col-sm-3">
                                <!-- <input type="text" class="form-control" name="golongandarah" id="golongandarah" placeholder="gol. darah ibu"> -->
                                <select class="form-control" name="golongandarah" id="golongandarah" required style="border-radius:0px">
                                    <option value="" selected>- gol. darah ibu -</option>
                                    <option value="O">O</option>
                                    <option value="A">A</option>
                                    <option value="B">B</option>
                                    <option value="AB">AB</option>
                                </select>
                            </div>
                            <div class="form-group col-sm-3">
                                <input type="text" class="form-control" name="telp" id="telp" placeholder="nomor telpon">
                            </div>
                            <div class="form-group col-sm-3">
                                <input type="text" class="form-control" name="pendidikanibu" id="pendidikanibu" placeholder="pendidikan ibu">
                            </div>
                            <div class="form-group col-sm-3">
                                <input type="text" class="form-control" name="pekerjaanibu" id="pekerjaanibu" placeholder="pekerjaan ibu">
                            </div>
                            <div class="form-group col-sm-3">
                                <input type="text" class="form-control" name="pendidikansuami" id="pendidikansuami" placeholder="pendidikan suami">
                            </div>
                            <div class="form-group col-sm-3">
                                <input type="text" class="form-control" name="pekerjaansuami" id="pekerjaansuami" placeholder="pekerjaan suami">
                            </div>
                            <div class="form-group col-sm-10">
                                <input type="text" class="form-control" name="alamat" id="alamat" placeholder="alamat">
                            </div>
                            <div class="form-group col-sm-2">
                                <input type="text" class="form-control" name="kodepos" id="kodepos" placeholder="kode pos">
                            </div>
                            <div class="form-group col-sm-12">
                                <hr>
                            </div>
                            <div class="form-group col-sm-6">
                                <input type="text" class="form-control" name="username" id="username" placeholder="username">
                            </div>
                            <div class="form-group col-sm-6">
                                <input type="password" class="form-control" name="password" id="password" placeholder="password">
                            </div>

                        </div>
                        <div class="form-group col-xs-12">
                            <div id='mail_success' class='success' style="display:none;">
                                Anda sudah berhasil mendaftar.
                            </div><!-- success message -->
                            <div id='mail_fail' class='error' style="display:none;">
                                Maaf anda gagal mendaftar.
                            </div><!-- error message -->
                        </div>
                        <div class="form-group col-sm-12">
                            <input type="submit" style="height:40px;" class="btn btn-lg costom-btn" value="daftar">
                        </div>
                    </form>
                </div>

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- get in touch -->

    <footer>
        <div class="footer-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3">
                        <a href="index.html"><img src="<?php echo base_url(); ?>assets/img/footer-logo.png" alt="ANAKSEHATKU"></a>
                        <p class="footer-content">Aplikasi gratis ditunjukkan untuk kemudahan Ibu dan Bidan.</p>
                    </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                    <div class="wow zoomIn col-xs-12 col-sm-3 col-md-3">
                        <p class="footer-heading">link</p>
                        <ul class="footermenu">
                            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#about">about us</a></li>
                            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#services">services</a></li>
                            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#product">product</a></li>
                            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#team">team</a></li>
                            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#blog">blog</a></li>
                        </ul>
                    </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                    <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3">
                        <p class="footer-heading">find us</p>
                        <ul class="footercontact">
                            <li><i class="flaticon-mainpage"></i><span>address:</span> One ANAKSEHATKU loop, 54100</li>
                            <li><i class="flaticon-phone16"></i><span>phone:</span><a href="tel:88 02 8714612"> +88 02 8714612</a></li>
                            <li><i class="flaticon-email21"></i><span>e-mail:</span><a href="mailto:support@themerole.com"> support@themerole.com</a></li>
                            <li><i class="flaticon-world91"></i><span>web:</span><a href="http://themerole.com"> www.themerole.com</a></li>
                        </ul>
                        <i class="flaticon-home78"></i>
                    </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                    <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3">
                        <p class="footer-heading">recent posts</p>
                        <ul class="footerblog">
                            <li><a href="blog-sidebar.html">The Green Fields of Spring</a> <p>13th Jun 2014</p></li>
                            <li><a href="blog-sidebar.html">This is a Video Post</a> <p>18th Nov 2014</p></li>
                            <li><a href="blog-sidebar.html">Satisfaction Lies in the Effort</a> <p>13th Jun 2014</p></li>
                        </ul>
                    </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="wow zoomIn col-xs-12">
                        <p>© 2015 All rights reserved. <span>ANAKSEHATKU</span> theme by <a href="http://themerole.com">themerole</a></p>
                        <div class="backtop  pull-right">
                            <i class="fa fa-angle-up back-to-top"></i>
                        </div><!-- /.backtop -->
                    </div><!-- /.col-xs-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.creditwrapper -->
    </footer><!-- /Footer -->

</body>
</html>
<!-- Modernizr JS -->
<script src="<?php echo base_url(); ?>assets/js/modernizr-2.6.2.min.js"></script>

<!--Bootatrap JS-->
<script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- REVOLUTION Slider  -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/rs-plugin/js/jquery.themepunch.revolution.js"></script>

<!-- Shuffle JS -->
<script src="<?php echo base_url(); ?>assets/js/jquery.shuffle.min.js"></script>

<!-- mmenu -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/mmenu/js/jquery.mmenu.min.js"></script>

<!-- Owl Carosel -->
<script src="<?php echo base_url(); ?>assets/vendor/owl/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>

<!-- waypoints JS-->
<script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>

<!-- Counterup JS -->
<script src="<?php echo base_url(); ?>assets/js/jquery.counterup.min.js"></script>

<!-- Easing JS -->
<script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>

<!-- Smooth Scroll JS -->
<script src="<?php echo base_url(); ?>assets/js/scrolling-nav.js"></script>
<script src="<?php echo base_url(); ?>assets/js/smoothscroll.min.js"></script>

<!-- Custom Script JS -->
<script src="<?php echo base_url(); ?>assets/js/script.js"></script>

<!-- Email JS -->
<script src="<?php echo base_url(); ?>assets/js/email.js"></script>
 
