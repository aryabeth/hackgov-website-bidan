<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Catatan Kesehatan</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,300,600,700' rel='stylesheet' type='text/css'>

    <!-- Revolution css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo base_url(); ?>assets/vendor/rs-plugin/css/extralayer.css">

    <!-- Flat icon css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/flat-icon/flaticon.css">

    <!-- Font awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css">

    <!-- Owl Carosel css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl/css/owl.theme.default.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl/css/owl.theme.css">

    <!-- mmenu -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/mmenu/css/jquery.mmenu.css" />

    <!-- Bootstrap css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.css">

    <!-- Animate css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">

    <!-- Custom Style css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/hover.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">


</head><!--/head-->

<body id="home" class="main">
    <header class="header-part">
        <div id="home" class="wrapper">
            <!-- Fixed navbar -->
            <div class="navi navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header page-scroll">
                        <a href="#menu">
                            <button type="button" data-effect="st-effect-1" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </a>
                    <a class="navbar-brand" href="index.html"><img src="<?php echo base_url(); ?>assets/img/logo2.png" alt="AnakSehatku"></a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse pull-right hidden-xs">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="page-scroll" href="<?php echo base_url(); ?>welcome#home">Home</a></li>
                            <li> <a class="page-scroll" href="<?php echo base_url(); ?>welcome#about">about us</a></li>
                            <li> <a class="page-scroll" href="<?php echo base_url(); ?>welcome#services">imunisasi</a></li>
                            <li> <a class="page-scroll" href="<?php echo base_url(); ?>welcome#product">Photo</a></li>
                            <li> <a class="page-scroll" href="<?php echo base_url(); ?>welcome#blog">info</a></li>
                            <li class="dropdown">
                                <a class="page-scroll drop dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" href="#blog">Nama Bidan</a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="<?php echo base_url(); ?>bidan/catatankesehatan">Catatan Kesehatan</a></li>
                                    <li><a href="<?php echo base_url(); ?>bidan/vitamin">Pemberian Vitamin</a></li>
                                    <li><a href="<?php echo base_url(); ?>bidan/deteksidini">Deteksi Dini</a></li>
                                    <li><a href="<?php echo base_url(); ?>bidan/lingkarkepala">Lingkar Kepala</a></li>
                                    <li><a href="<?php echo base_url(); ?>bidan/beratbadan">Berat Badan</a></li>
                                    <li><a href="<?php echo base_url(); ?>bidan/rekammedis">Catatan Penyakit</a></li>
                                        <li><a href="<?php echo base_url(); ?>bidan/broadcast">SMS Broadcast</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php echo base_url(); ?>login/logout">Logout</a></li>
                            
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div><!-- End of Nav -->
        </div>
    </header>

    <section id="daftar" class="contact-wrapper section-padding">
        <div class="container">
            <div class="row">
                <div class="wow zoomIn col-xs-12 text-center p-padding">
                    <h1 class="section-title">Catatan Pemberian Vitamin</h1>
                    <p>Isikan form berikut ini dengan data yang benar</p>
                </div><!-- col-xs-12 -->
                <div class="wow zoomIn col-xs-12 col-sm-12 col-md-8 col-md-offset-2">
                    <form name="contactForm" id='contact_form' method="post" action="<?php echo base_url(); ?>bidan/s_vitamin">
                        <div class="form-inline">
                            <div class="form-group col-sm-12">
                                <input  date-format="DMY"  class="form-control" name="tanggal" id="tanggal" required placeholder="tanggal">
                            </div>
                            <div class="form-group col-sm-12">
                                <input required="required" type="text" class="form-control" name="umur" id="umur" required placeholder="umur">
                                <input type="text" class="form-control" name="pasien" id="pasien" value="nama pasien" style="display:none;">
                                <input type="text" class="form-control" name="bidan" id="bidan" value="nama bidan" style="display:none;">
                            
                            </div>
                        </div>

                        <div class="form-group col-xs-12">
                            <div id='mail_success' class='success' style="display:none;">
                                Anda sudah berhasil mendaftar.
                            </div><!-- success message -->
                            <div id='mail_fail' class='error' style="display:none;">
                                Maaf anda gagal mendaftar.
                            </div><!-- error message -->
                        </div>
                        <div class="form-group col-sm-12">
                            <input type="submit" style="height:40px;" class="btn btn-lg costom-btn" value="simpan">
                        </div>
                    </form>
                </div>

            </div><!-- /.row -->
        </div><!-- /.container -->
    </section><!-- get in touch -->

    <footer>
        <div class="footer-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3">
                        <a href="index.html"><img src="<?php echo base_url(); ?>assets/img/footer-logo.png" alt="ANAKSEHATKU"></a>
                        <p class="footer-content">Aplikasi gratis ditunjukkan untuk kemudahan Ibu dan Bidan.</p>
                    </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                                     <!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                    <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3">
                        <p class="footer-heading">find us</p>
                        <ul class="footercontact">
                            <li><i class="flaticon-mainpage"></i><span>address:</span> One ANAKSEHATKU loop, 54100</li>
                            <li><i class="flaticon-phone16"></i><span>phone:</span><a href="tel:88 02 8714612"> +88 02 8714612</a></li>
                            <li><i class="flaticon-email21"></i><span>e-mail:</span><a href="mailto:support@themerole.com"> support@themerole.com</a></li>
                            <li><i class="flaticon-world91"></i><span>web:</span><a href="http://themerole.com"> www.themerole.com</a></li>
                        </ul>
                        <i class="flaticon-home78"></i>
                    </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                    <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3">
                        <p class="footer-heading">recent posts</p>
                        <ul class="footerblog">
                            <li><a href="blog-sidebar.html">The Green Fields of Spring</a> <p>13th Jun 2014</p></li>
                            <li><a href="blog-sidebar.html">This is a Video Post</a> <p>18th Nov 2014</p></li>
                            <li><a href="blog-sidebar.html">Satisfaction Lies in the Effort</a> <p>13th Jun 2014</p></li>
                        </ul>
                    </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="wow zoomIn col-xs-12">
                        <p>© 2015 All rights reserved. <span>ANAKSEHATKU</span> theme by <a href="http://themerole.com">themerole</a></p>
                        <div class="backtop  pull-right">
                            <i class="fa fa-angle-up back-to-top"></i>
                        </div><!-- /.backtop -->
                    </div><!-- /.col-xs-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.creditwrapper -->
    </footer><!-- /Footer -->

</body>
</html>

 <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.js"></script>

    <!-- Modernizr JS -->
    <script src="<?php echo base_url(); ?>assets/js/modernizr-2.6.2.min.js"></script>

    <!--Bootatrap JS-->
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- REVOLUTION Slider  -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/rs-plugin/js/jquery.themepunch.revolution.js"></script>

    <!-- Shuffle JS -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.shuffle.min.js"></script>

    <!-- mmenu -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/mmenu/js/jquery.mmenu.min.js"></script>

    <!-- Owl Carosel -->
    <script src="<?php echo base_url(); ?>assets/vendor/owl/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>

    <!-- waypoints JS-->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>

    <!-- Counterup JS -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.counterup.min.js"></script>

    <!-- Easing JS -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>

    <!-- Smooth Scroll JS -->
    <script src="<?php echo base_url(); ?>assets/js/scrolling-nav.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/smoothscroll.min.js"></script>

    <!-- Custom Script JS -->
    <script src="<?php echo base_url(); ?>assets/js/script.js"></script>

    <!-- Email JS -->
    <script src="<?php echo base_url(); ?>assets/js/email.js"></script>
