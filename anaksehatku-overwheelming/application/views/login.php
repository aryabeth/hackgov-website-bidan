<style type="text/css">
    html {
      width: 100%;
      height: 100%;
    }

    body {
      background: -webkit-linear-gradient(45deg, rgba(66, 183, 245, 0.8) 0%, rgba(66, 245, 189, 0.4) 100%);
      background: linear-gradient(45deg, rgba(66, 183, 245, 0.8) 0%, rgba(66, 245, 189, 0.4) 100%);
      color: rgba(0, 0, 0, 0.6);
      font-family: "Roboto", sans-serif;
      font-size: 14px;
      line-height: 1.6em;
      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
    }

    .overlay, .form-panel.one:before {
      position: absolute;
      top: 0;
      left: 0;
      display: none;
      background: rgba(0, 0, 0, 0.8);
      width: 100%;
      height: 100%;
    }

    .form {
      z-index: 15;
      position: relative;
      background: #FFFFFF;
      width: 600px;
      border-radius: 4px;
      box-shadow: 0 0 30px rgba(0, 0, 0, 0.1);
      box-sizing: border-box;
      margin: 100px auto 10px;
      overflow: hidden;
    }
    .form-toggle {
      z-index: 10;
      position: absolute;
      top: 60px;
      right: 60px;
      background: #FFFFFF;
      width: 60px;
      height: 60px;
      border-radius: 100%;
      -webkit-transform-origin: center;
          -ms-transform-origin: center;
              transform-origin: center;
      -webkit-transform: translate(0, -25%) scale(0);
          -ms-transform: translate(0, -25%) scale(0);
              transform: translate(0, -25%) scale(0);
      opacity: 0;
      cursor: pointer;
      -webkit-transition: all 0.3s ease;
              transition: all 0.3s ease;
    }
    .form-toggle:before, .form-toggle:after {
      content: '';
      display: block;
      position: absolute;
      top: 50%;
      left: 50%;
      width: 30px;
      height: 4px;
      background: #f9844c;
      -webkit-transform: translate(-50%, -50%);
          -ms-transform: translate(-50%, -50%);
              transform: translate(-50%, -50%);
    }
    .form-toggle:before {
      -webkit-transform: translate(-50%, -50%) rotate(45deg);
          -ms-transform: translate(-50%, -50%) rotate(45deg);
              transform: translate(-50%, -50%) rotate(45deg);
    }
    .form-toggle:after {
      -webkit-transform: translate(-50%, -50%) rotate(-45deg);
          -ms-transform: translate(-50%, -50%) rotate(-45deg);
              transform: translate(-50%, -50%) rotate(-45deg);
    }
    .form-toggle.visible {
      -webkit-transform: translate(0, -25%) scale(1);
          -ms-transform: translate(0, -25%) scale(1);
              transform: translate(0, -25%) scale(1);
      opacity: 1;
    }
    .form-group {
      display: -webkit-box;
      display: -webkit-flex;
      display: -ms-flexbox;
      display: flex;
      -webkit-flex-wrap: wrap;
          -ms-flex-wrap: wrap;
              flex-wrap: wrap;
      -webkit-box-pack: justify;
      -webkit-justify-content: space-between;
          -ms-flex-pack: justify;
              justify-content: space-between;
      margin: 0 0 20px;
    }
    .form-group:last-child {
      margin: 0;
    }
    .form-group label {
      display: block;
      margin: 0 0 10px;
      color: rgba(0, 0, 0, 0.6);
      font-size: 12px;
      font-weight: 500;
      line-height: 1;
      text-transform: uppercase;
      letter-spacing: .2em;
    }
    .two .form-group label {
      color: #FFFFFF;
    }
    .form-group input {
      outline: none;
      display: block;
      background: rgba(0, 0, 0, 0.1);
      width: 100%;
      border: 0;
      border-radius: 4px;
      box-sizing: border-box;
      padding: 12px 20px;
      color: rgba(0, 0, 0, 0.6);
      font-family: inherit;
      font-size: inherit;
      font-weight: 500;
      line-height: inherit;
      -webkit-transition: 0.3s ease;
              transition: 0.3s ease;
    }
    .form-group input:focus {
      color: rgba(0, 0, 0, 0.8);
    }
    .two .form-group input {
      color: #FFFFFF;
    }
    .two .form-group input:focus {
      color: #FFFFFF;
    }
    .form-group button {
      outline: none;
      background: #f9844c;
      width: 100%;
      border: 0;
      border-radius: 4px;
      padding: 12px 20px;
      color: #FFFFFF;
      font-family: inherit;
      font-size: inherit;
      font-weight: 500;
      line-height: inherit;
      text-transform: uppercase;
      cursor: pointer;
    }
    .two .form-group button {
      background: #FFFFFF;
      color: #f9844c;
    }
    .form-group .form-remember {
      font-size: 12px;
      font-weight: 400;
      letter-spacing: 0;
      text-transform: none;
    }
    .form-group .form-remember input[type='checkbox'] {
      display: inline-block;
      width: auto;
      margin: 0 10px 0 0;
    }
    .form-group .form-recovery {
      color: #f9844c;
      font-size: 12px;
      text-decoration: none;
    }
    .form-panel {
      padding: 60px calc(5% + 60px) 60px 60px;
      box-sizing: border-box;
    }
    .form-panel.one:before {
      content: '';
      display: block;
      opacity: 0;
      visibility: hidden;
      -webkit-transition: 0.3s ease;
              transition: 0.3s ease;
    }
    .form-panel.one.hidden:before {
      display: block;
      opacity: 1;
      visibility: visible;
    }
    .form-panel.two {
      z-index: 5;
      position: absolute;
      top: 0;
      left: 95%;
      background: #f9844c;
      width: 100%;
      min-height: 100%;
      padding: 60px calc(10% + 60px) 60px 60px;
      -webkit-transition: 0.3s ease;
              transition: 0.3s ease;
      cursor: pointer;
    }
    .form-panel.two:before, .form-panel.two:after {
      content: '';
      display: block;
      position: absolute;
      top: 60px;
      left: 1.5%;
      background: rgba(255, 255, 255, 0.2);
      height: 30px;
      width: 2px;
      -webkit-transition: 0.3s ease;
              transition: 0.3s ease;
    }
    .form-panel.two:after {
      left: 3%;
    }
    .form-panel.two:hover {
      left: 93%;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
    }
    .form-panel.two:hover:before, .form-panel.two:hover:after {
      opacity: 0;
    }
    .form-panel.two.active {
      left: 10%;
      box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
      cursor: default;
    }
    .form-panel.two.active:before, .form-panel.two.active:after {
      opacity: 0;
    }
    .form-header {
      margin: 0 0 40px;
    }
    .form-header h1 {
      padding: 4px 0;
      color: #f9844c;
      font-size: 24px;
      font-weight: 700;
      text-transform: uppercase;
    }
    .two .form-header h1 {
      position: relative;
      z-index: 40;
      color: #FFFFFF;
    }

    
    .cp-fab {
      background: #FFFFFF !important;
      color: #f9844c !important;
    }

</style>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,300,600,700' rel='stylesheet' type='text/css'>

    <!-- Revolution css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo base_url(); ?>assets/vendor/rs-plugin/css/extralayer.css">

    <!-- Flat icon css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/flat-icon/flaticon.css">

    <!-- Font awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css">

    <!-- Owl Carosel css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl/css/owl.theme.default.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl/css/owl.theme.css">

    <!-- mmenu -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/mmenu/css/jquery.mmenu.css" />

    <!-- Bootstrap css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.css">

    <!-- Animate css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">

    <!-- Custom Style css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/hover.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">


</head><!--/head-->

<body id="home" class="main">
    <header class="header-part">
        <div id="home" class="wrapper">
            <!-- Fixed navbar -->
            <div class="navi navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header page-scroll">
                        <a href="#menu">
                            <button type="button" data-effect="st-effect-1" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </a>
                        <a class="navbar-brand" href="index.html"><img src="<?php echo base_url(); ?>assets/img/logo2.png" alt="AnakSehatku"></a>
                      </div>
                    <div id="navbar" class="navbar-collapse collapse pull-right hidden-xs">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="<?php echo base_url(); ?>welcome#home">Home</a></li>
                            <li> <a href="<?php echo base_url(); ?>welcome#about">about us</a></li>
                            <li> <a href="<?php echo base_url(); ?>welcome#services">imunisasi</a></li>
                            <li> <a href="<?php echo base_url(); ?>welcome#product">product</a></li>
                            <li> <a href="<?php echo base_url(); ?>welcome#blog">info</a></li>
                            <li><a href="<?php echo base_url(); ?>login">Login</a></li>
                            <!-- Search Block -->
                            <li>
                                <i class="search fa fa-search search-btn"></i>
                                <div class="search-open" style="display:none;">
                                    <div class="input-group animated fadeInDown">
                                        <input type="text" class="form-control" placeholder="Search">
                                        <span class="input-group-btn">
                                            <button class="btn-u" type="button">Go</button>
                                        </span>
                                    </div>
                                </div>
                            </li>
                            <!-- End Search Block -->
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div><!-- End of Nav -->
        </div>
    </header>

    <section id="login" class="contact-wrapper">
        <div class="container">
            <!-- Form-->
            <div class="form">
              <div class="form-toggle"></div>
                <div class="form-panel one">
                  <center>
                    <div class="form-header">
                      <h1>Account Login</h1>
                    </div>
                    <div class="form-content">
                      <form method="post" action="<?php echo base_url(); ?>login/proseslogin">
                        <div class="form-group">
                          <label for="username">Username</label>
                          <input type="text" id="username" name="username" required="required" placeholder="username"/>
                        </div>
                        <div class="form-group">
                          <label for="password">Password</label>
                          <input type="password" id="password" name="password" required="required" placeholder="password"/>
                        </div>
                        <div class="form-group">
                          <label class="form-remember">
                            <input type="checkbox"/>Remember Me
                          </label><a href="<?php echo base_url(); ?>daftar" class="form-recovery">Belum punya akun?</a>
                        </div>
                        <div class="form-group">
                          <button type="submit">Log In</button>
                        </div>
                      </form>
                    </div>
                  </center>
                </div>
            </div>
            <br><br>
        </div>
    </section>

    <footer>
        <div class="footer-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3">
                        <a href="index.html"><img src="<?php echo base_url(); ?>assets/img/footer-logo.png" alt="TechGut"></a>
                        <p class="footer-content">Aplikasi gratis ditunjukkan untuk kemudahan Ibu dan Bidan.</p>
                    </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                    <div class="wow zoomIn col-xs-12 col-sm-3 col-md-3">
                        <p class="footer-heading">link</p>
                        <ul class="footermenu">
                            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#about">about us</a></li>
                            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#services">services</a></li>
                            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#product">product</a></li>
                            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#team">team</a></li>
                            <li><a data-scroll data-options='{ "easing": "easeInQuad" }' href="#blog">blog</a></li>
                        </ul>
                    </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                    <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3">
                        <p class="footer-heading">find us</p>
                        <ul class="footercontact">
                            <li><i class="flaticon-mainpage"></i><span>address:</span> One TECHGUT loop, 54100</li>
                            <li><i class="flaticon-phone16"></i><span>phone:</span><a href="tel:88 02 8714612"> +88 02 8714612</a></li>
                            <li><i class="flaticon-email21"></i><span>e-mail:</span><a href="mailto:support@themerole.com"> support@themerole.com</a></li>
                            <li><i class="flaticon-world91"></i><span>web:</span><a href="http://themerole.com"> www.themerole.com</a></li>
                        </ul>
                        <i class="flaticon-home78"></i>
                    </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                    <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3">
                        <p class="footer-heading">recent posts</p>
                        <ul class="footerblog">
                            <li><a href="blog-sidebar.html">The Green Fields of Spring</a> <p>13th Jun 2014</p></li>
                            <li><a href="blog-sidebar.html">This is a Video Post</a> <p>18th Nov 2014</p></li>
                            <li><a href="blog-sidebar.html">Satisfaction Lies in the Effort</a> <p>13th Jun 2014</p></li>
                        </ul>
                    </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="wow zoomIn col-xs-12">
                        <p>© 2015 All rights reserved. <span>techgut</span> theme by <a href="http://themerole.com">themerole</a></p>
                        <div class="backtop  pull-right">
                            <i class="fa fa-angle-up back-to-top"></i>
                        </div><!-- /.backtop -->
                    </div><!-- /.col-xs-12 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.creditwrapper -->
    </footer><!-- /Footer -->


</body>
</html>

<script type="text/javascript">
    $(document).ready(function() {
    var panelOne = $('.form-panel.two').height(),
      panelTwo = $('.form-panel.two')[0].scrollHeight;

    $('.form-panel.two').not('.form-panel.two.active').on('click', function(e) {
      e.preventDefault();

      $('.form-toggle').addClass('visible');
      $('.form-panel.one').addClass('hidden');
      $('.form-panel.two').addClass('active');
      $('.form').animate({
        'height': panelTwo
      }, 200);
    });

    $('.form-toggle').on('click', function(e) {
      e.preventDefault();
      $(this).removeClass('visible');
      $('.form-panel.one').removeClass('hidden');
      $('.form-panel.two').removeClass('active');
      $('.form').animate({
        'height': panelOne
      }, 200);
    });
  });

</script>