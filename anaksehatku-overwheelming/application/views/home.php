<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js">
 <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <title>Anak Sehatku</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png" />

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,300,600,700' rel='stylesheet' type='text/css'>

    <!-- Revolution css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/vendor/rs-plugin/css/settings.css" media="screen" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/<?php echo base_url(); ?>assets/vendor/rs-plugin/css/extralayer.css">

    <!-- Flat icon css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/flat-icon/flaticon.css">

    <!-- Font awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/font-awesome/css/font-awesome.min.css">

    <!-- Owl Carosel css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl/css/owl.theme.default.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/owl/css/owl.theme.css">

    <!-- mmenu -->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/mmenu/css/jquery.mmenu.css" />

    <!-- Bootstrap css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/vendor/bootstrap/css/bootstrap.css">

    <!-- Animate css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/animate.css">

    <!-- Custom Style css -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/hover.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.css">

    <!--[if lt IE 9]>
    <script src="<?php echo base_url(); ?>assets///html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <script>window.html5 || document.write('<script src="<?php echo base_url(); ?>assets/js/vendor/html5shiv.js"><\/script>')
    </script>
    <![endif]-->
</head>
<body>
    <div class="main" id="home">
        <!-- slider -->
        <div class="tp-banner-container">
            <div class="tp-banner">
                <ul>
                    <!-- SLIDE  -->
                    <li data-transition="fade" data-slotamount="25" data-masterspeed="1000" data-thumb="<?php echo base_url(); ?>assets/img/slider/slider_img_04.jpg" data-saveperformance="off">
                        <!-- MAIN IMAGE -->
                        <img src="<?php echo base_url(); ?>assets/img/slider/slider_img_01.jpg" alt="fullslide2" data-bgposition="center center" data-kenburns="on" data-duration="12000" data-ease="Power0.easeInOut" data-bgfit="115" data-bgfitend="100" data-bgpositionend="center center">
                        <div class="tp-caption very_large_text lfb ltt skewfromrightshort fadeout tp-resizeme head-tag"
                             data-x="637"
                             data-y="100"
                             data-speed="1500"
                             data-start="1500"
                             data-easing="Power3.easeInOut"
                             data-splitin="chars"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.05"
                             data-endspeed="300"
                             style="z-index: 5; max-width: auto; max-height: auto; font-size:63px;white-space: nowrap;color:#fff;">
                            <h1>SUDAHKAH</h1>
                        </div>
                        <div class="tp-caption very_large_text lfb ltt skewfromrightshort fadeout tp-resizeme head-tag"
                             data-x="1000"
                             data-y="100"
                             data-speed="1500"
                             data-start="1500"
                             data-easing="Power3.easeInOut"
                             data-splitin="chars"
                             data-splitout="none"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.1"
                             data-endspeed="300"
                             style="z-index: 5; max-width: auto; max-height: auto; font-size:63px;white-space: nowrap;color:#f9844c;">
                            <h1>ANAK</h1>
                        </div>

                        <!-- LAYERS -->
                        <div class="tp-caption very_large_text lfb ltt skewfromrightshort fadeout tp-resizeme"
                             data-x="637"
                             data-y="182"
                             data-speed="1500"
                             data-start="1500"
                             data-easing="Power3.easeInOut"
                             data-splitin="chars"
                             data-splitout="none"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.1"
                             data-endspeed="300"
                             style="z-index: 5; max-width: auto; max-height: auto; font-size:63px;white-space: nowrap;color:#f9844c;">
                            <h1>ANDA</h1>
                        </div>
                        <div class="tp-caption very_large_text lfb ltt skewfromrightshort fadeout tp-resizeme"
                             data-x="825"
                             data-y="182"
                             data-speed="1500"
                             data-start="1500"
                             data-easing="Power3.easeInOut"
                             data-splitin="chars"
                             data-splitout="none"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.1"
                             data-endspeed="300"
                             style="z-index: 5; max-width: auto; max-height: auto; font-size:63px;white-space: nowrap;color:#fff;">
                            <h1>IMUNISASI?</h1>
                        </div>

                          <img src="<?php echo base_url(); ?>assets/img/slider/slider_img_01.jpg" alt="fullslide2" data-bgposition="center center" data-kenburns="on" data-duration="12000" data-ease="Power0.easeInOut" data-bgfit="115" data-bgfitend="100" data-bgpositionend="center center">
                        <div class="tp-caption very_large_text lfb ltt skewfromrightshort fadeout tp-resizeme head-tag"
                             data-x="50"
                             data-y="400"
                             data-speed="3000"
                             data-start="3000"
                             data-easing="Power3.easeInOut"
                             data-splitin="chars"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.05"
                             data-endspeed="300"
                             style="z-index: 5; max-width: auto; max-height: auto; font-size:63px;white-space: nowrap;color:#fff;">
                            <h1>SUDAHKAH</h1>
                        </div>
                        <div class="tp-caption very_large_text lfb ltt skewfromrightshort fadeout tp-resizeme head-tag"
                             data-x="400"
                             data-y="400"
                             data-speed="3000"
                             data-start="3000"
                             data-easing="Power3.easeInOut"
                             data-splitin="chars"
                             data-splitout="none"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.1"
                             data-endspeed="300"
                             style="z-index: 5; max-width: auto; max-height: auto; font-size:63px;white-space: nowrap;color:#f9844c;">
                            <h1>ANDA</h1>
                        </div>

                        <!-- LAYERS -->
                        <div class="tp-caption very_large_text lfb ltt skewfromrightshort fadeout tp-resizeme"
                             data-x="50"
                             data-y="475"
                             data-speed="3000"
                             data-start="3000"
                             data-easing="Power3.easeInOut"
                             data-splitin="chars"
                             data-splitout="none"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.1"
                             data-endspeed="300"
                             style="z-index: 5; max-width: auto; max-height: auto; font-size:63px;white-space: nowrap;color:#f9844c;">
                            <h1>MEMANTAU</h1>
                        </div>
                        <div class="tp-caption very_large_text lfb ltt skewfromrightshort fadeout tp-resizeme"
                             data-x="410"
                             data-y="475"
                             data-speed="3000"
                             data-start="3000"
                             data-easing="Power3.easeInOut"
                             data-splitin="chars"
                             data-splitout="none"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.1"
                             data-endspeed="300"
                             style="z-index: 5; max-width: auto; max-height: auto; font-size:63px;white-space: nowrap;color:#fff;">
                            <h1>PERKEMBANGAN</h1>
                        </div>
                        <div class="tp-caption very_large_text lfb ltt skewfromrightshort fadeout tp-resizeme"
                             data-x="50"
                             data-y="550"
                             data-speed="3000"
                             data-start="3000"
                             data-easing="Power3.easeInOut"
                             data-splitin="chars"
                             data-splitout="none"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.1"
                             data-endspeed="300"
                             style="z-index: 5; max-width: auto; max-height: auto; font-size:63px;white-space: nowrap;color:#fff;">
                            <h1>BUAH HATI?</h1>
                        </div>
                        
                    <li class="items" data-transition="slidevertical" data-slotamount="1" data-masterspeed="1500" data-thumb="<?php echo base_url(); ?>assets/img/slider/slider_img_03.jpg" data-delay="13000" data-saveperformance="off">
                        <!-- MAIN IMAGE -->
                        <img src="<?php echo base_url(); ?>assets/img/slider/image1.jpg" alt="kenburns1" data-bgposition="left center" data-kenburns="on" data-duration="14000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="130" data-bgpositionend="right center">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption very_large_text lfb ltt tp-resizeme head-tag"
                             data-x="center" data-hoffset="0"
                             data-y="center" data-voffset="-160"
                             data-speed="1200"
                             data-start="1700"
                             data-easing="Power3.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                            <h1><span>Apa PENTINGNYA  </span> IMUNISASI DAN </h1>
                            <h1><span>  PEMANTAUAN KESEHATAN </span> BAGI ANAK?  </span> 
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption  lfl tp-resizeme small-text"
                             data-x="center" data-hoffset="0"
                             data-y="center" data-voffset="-60"
                             data-speed="1500"
                             data-start="2000"
                             data-easing="Power1.easeInOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-endspeed="300"
                             style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap; ">
                            <p style="font-color:#000;">Imunisasi atau vaksin merupakan investasi
                             masa depan bagi anak, karena dengan vaksin anak akan terhindar dari
                             </p>
                        </div>
                          <!-- LAYER NR. 2 -->
                        <div class="tp-caption  lfl tp-resizeme small-text"
                             data-x="200" data-hoffset="0"
                             data-y="280" data-voffset="-60"
                             data-speed="1500"
                             data-start="2000"
                             data-easing="Power1.easeInOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-endspeed="300"
                             style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap; ">
                            <p style="font-color:#000;">
                             penyakit serta infeksi berbahaya.
                             pemantauan memiliki peran penting, bukan untuk mengobati tetapi untuk mencegah.</p>
                        </div>
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption  lfl tp-resizeme small-text"
                             data-x="100" data-hoffset="0"
                             data-y="300" data-voffset="-30"
                             data-speed="1500"
                             data-start="2000"
                             data-easing="Power1.easeInOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-endspeed="300"
                             style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap; color:#f9844c;">
                            <p>Anak-anak Anda akan memiliki kesempatan beraktifitas, bermain dan belajar tanpa terganggu oleh masalah kesehatan.</p>
                        </div>

                        <!-- LAYER NR. 5 -->
                        <div class="lfr tp-caption tp-resizeme start-button"
                             data-x="440" data-hoffset="90"
                             data-y="400" data-voffset="33"
                             data-speed="2500"
                             data-start="4000"
                             data-easing="Power4.easeInOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.1"
                             data-endelementdelay="0.1"
                             data-endspeed="1000"
                             data-endeasing="Power1.easeOut"
                             style="z-index: 3; max-width: auto; max-height: auto; white-space: nowrap;">
                            <center><button type="button" class="btn btn-primary" style="background-color: #e84700;border:1px solid #e95e1d;">MARI BERGABUNG!</button></center>
                        </div>
                        <!-- LAYER NR. 6 -->
                        <div class="tp-caption tp-resizeme lfb randomrotate down-arrow"
                             data-x="center" data-hoffset="0"
                             data-y="center" data-voffset="230"
                             data-speed="2200"
                             data-start="5500"
                             data-easing="Power4.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.01"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 3; max-width: 50px; max-height: 50px; white-space: nowrap;">
                            <i class="scroll-bottom"><img src="<?php echo base_url(); ?>assets/img/slider_button.png" alt="arrow-down.png" class="img-responsive"></i>
                        </div>
                    </li>

                    <li class="items" data-transition="slideleft" data-slotamount="1" data-masterspeed="1500" data-thumb="<?php echo base_url(); ?>assets/img/slider/slider_img_02.jpg" data-delay="13000" data-saveperformance="on">
                        <!-- MAIN IMAGE -->
                        <img src="<?php echo base_url(); ?>assets/img/slider/slider_img_02.jpg" alt="kenburns1" data-bgposition="left center" data-kenburns="on" data-duration="14000" data-ease="Linear.easeNone" data-bgfit="100" data-bgfitend="130" data-bgpositionend="right center">
                        <!-- LAYERS -->
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption very_large_text lfb ltt tp-resizeme head-tag"
                             data-x="left" data-hoffset="20"
                             data-y="center" data-voffset="-100"
                             data-speed="600"
                             data-start="500"
                             data-easing="Power3.easeInOut"
                             data-splitin="chars"
                             data-splitout="chars"
                             data-elementdelay="0.08"
                             style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                            <h1><span>PANTAU BUAH HATI KESAYANGAN ANDA!</span></h1>
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class=" tp-caption  lfl tp-resizeme"
                             data-x="-100"
                             data-y="340"
                             data-speed="500"
                             data-start="1500"
                             data-easing="Power3.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                            <h2 class="small-title">PERIKSA BUAH HATI MELALUI BIDAN TERDEKAT</h2>
                        </div>

                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption  lfl tp-resizeme"
                             data-x="-100"
                             data-y="400"
                             data-speed="1000"
                             data-start="2000"
                             data-easing="Power3.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                            <h2 class="small-title">CUKUP MEMBAWA APLIKASI INI SAAT MEMERIKSA </h2>
                        </div>

                        <!-- LAYER NR. 4 -->
                        <div class="tp-caption  lfl tp-resizeme"
                             data-x="-100"
                             data-y="460"
                             data-speed="2000"
                             data-start="2500"
                             data-easing="Power3.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                            <h2 class="small-title"> MEMUDAHKAN IBU DAN BIDAN, GRATIS!</h2>
                        </div>

                        <!-- LAYER NR. 5 -->
                        <div class="tp-caption lfr tp-resizeme"
                             data-x="right" data-hoffset="100"
                             data-y="bottom" data-voffset="-90"
                             data-speed="3000"
                             data-start="4000"
                             data-easing="Power3.easeOut"
                             data-splitin="none"
                             data-splitout="none"
                             data-elementdelay="0.05"
                             data-endelementdelay="0.1"
                             data-endspeed="500"
                             data-endeasing="Power4.easeIn"
                             style="z-index: 2; max-width: auto; max-height: auto; white-space: nowrap;">
                            <button type="button" class="btn btn-default buy-btn">DAFTAR SEKARANG</button>
                        </div>
                       
                    </li>
                    
                </ul>
            </div>
        </div>
        <!-- slider -->
        <header class="header-part">
            <div id="home" class="wrapper">
                <!-- Fixed navbar -->
                <div class="navi navbar-default" role="navigation">
                    <div class="container">
                        <div class="navbar-header page-scroll">
                            <a href="#menu">
                                <button type="button" data-effect="st-effect-1" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </a>
                            <a class="navbar-brand" href="index.html"><img src="<?php echo base_url(); ?>assets/img/logo2.png" alt="AnakSehatku"></a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse pull-right hidden-xs">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a class="page-scroll" href="#home">Home</a></li>
                                <li> <a class="page-scroll" href="#about">about us</a></li>
                                <li> <a class="page-scroll" href="#services">imunisasi</a></li>
                                <li> <a class="page-scroll" href="#product">foto</a></li>
                                <li> <a class="page-scroll" href="#blog">info</a></li>
                                <li> <a href="<?php echo base_url(); ?>login">Login</a></li>
                                <!-- Search Block -->
                                <li>
                                    <i class="search fa fa-search search-btn"></i>
                                    <div class="search-open" style="display:none;">
                                        <div class="input-group animated fadeInDown">
                                            <input type="text" class="form-control" placeholder="Search">
                                            <span class="input-group-btn">
                                                <button class="btn-u" type="button">Go</button>
                                            </span>
                                        </div>
                                    </div>
                                </li>
                                <!-- End Search Block -->
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </div><!-- End of Nav -->
            </div>
        </header>

        
<!-- WHO WE ARE -->
<section id="about" class="who-we-are section-padding">
  <div class="container">
    <div class="row">
      <div class="wow zoomIn col-xs-12 col-lg-12 text-center p-padding">
        <h1 class="section-title">AnakSehatku</h1>
        <p>Pantau Buah Hati merupakan aplikasi web untuk membantu para Ibu dalam memantau perkembangan buah hatinya
         <br> dan memudahkan para Bidan untuk pencatatan administrasi.</p>
      </div><!-- col-xs-12 -->
      <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3 text-center">
        <div class="icon-circle">
          <i class="fa fa-child"></i>
        </div><!-- /.icon-circle -->
        <div class="who-we-are-content text-center">
          <h2>TIMBANG BERAT BADAN</h2>
          <p>Moms, jangan lupa pantau berat badan kesayangan ya. Saat periksa ke Ibu Bidan, cukup bawa aplikasi ini untuk di catat oleh Ibu Bidan.</p>
        </div><!-- /.who-we-are-content -->
      </div><!-- /.col-xs-6 -->
      <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3 text-center">
        <div class="icon-circle">
          <i class="fa fa-laptop"></i>
        </div><!-- /.icon-circle -->
        <div class="who-we-are-content text-center">
          <h2>PANTAU KESEHATAN ANAK</h2>
          <p>Fitur pantau kesehatan mempermudah para Ibu memantau kesehatan sang buah hati tanpa terbatas ruang dan waktu. </p>
        </div><!-- /.who-we-are-content -->
      </div><!-- /.col-xs-6 -->
      <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3 text-center">
        <div class="icon-circle">
          <i class="fa fa-mobile-phone"></i>
        </div><!-- /.icon-circle -->
        <div class="who-we-are-content text-center">
          <h2>SMS Gateway Imunisasi</h2>
          <p>Dear Ibu, Imunisasi itu sangat penting, jangan lupa bawa buah hati kesayangan ke Pos Pelayanan terdekat ya. Jika Ibu lupa, pantau buah hati akan mengingatkan Ibu untuk segera melakukan imunisasi sesuai usia Buah Hati kesayangan, Ibu bidan tinggal menunggu Ibu.</p>
        </div><!-- /.who-we-are-content -->
      </div><!-- /.col-xs-6 -->
      <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3 text-center">
        <div class="icon-circle">
          <i class="fa fa-thumbs-up"></i>
        </div><!-- /.icon-circle -->
        <div class="who-we-are-content text-center">
          <h2>Portable</h2>
          <p>Jika buku pencatatan kesehatan buah hati hilang, Ibu tidak perlu kuatir, cukup membawa aplikasi ini, Ibu tetap dapat mencatat rekaman kesehatan sang buah hati. Ibu bidan juga terbantu lho, karena tidak kesulitan dalam mencari data buah hati.</p>
        </div><!-- /.who-we-are-content -->
      </div><!-- /.col-xs-6 -->
    </div><!--/row-->
  </div><!-- /container -->
</section><!-- /who we wre -->
       <!-- OUR SERVICES -->
<section id="services" class="service section-padding">
  <div class="container">
    <div class="row">
      <div class="wow zoomIn col-xs-12 text-center p-padding">
        <h1 class="section-title">Pantau perkembangan dan imunisasi, apakah penting?</h1>
        <p>Sangat penting, pemantauan perkembangan adalah untuk mengetahui adanya gangguan perkembangan seorang bayi atau anak sebelum gangguan itu terjadi, jadi pemantauan ini tidak dimaksudkan untuk mengobati gangguan perkembangan yang telah terjadi. Lalu, bagaimana dengan imunisasi?</p>
      </div><!-- col-xs-12 -->
      <div class="col-xs-12 col-sm-5 col-md-5">
        <div class="left-column">
          <div class="wow zoomIn media">
            <div class="media-left media-middle">
              <a href="#">
                <i class="fa fa-heartbeat"></i>
              </a>
            </div>
            <div class="media-body">
              <h2>Hepatitis B</h2>
              <h3>Mencegah hepatitis B</h3>
              <span>(penyakit hati)</span>
            </div>
          </div>
          <div class="wow zoomIn media">
            <div class="media-left media-middle">
              <a href="#">
                <i class="fa fa-stethoscope"></i>
              </a>
            </div>
            <div class="media-body">
              <h2>BCG</h2>
              <h3>Mencegah TBC/ Tuberkulosis</h3>
              <span>(sakit paru-paru)</span>
            </div>
          </div>
          <div class="wow zoomIn media">
            <div class="media-left media-middle">
              <a href="#">
                <i class="fa fa-medkit"></i>
              </a>
            </div>
            <div class="media-body">
              <h2>Polio</h2>
              <h3>Mencegah polio (lumpuh layuh pada tungkai kaki</h3>
              <span>dan lengan tangan)</span>
            </div>
          </div>
        </div>
      </div><!-- col-md-5 -->

                    <div class="col-xs-6 col-sm-2 col-md-2 hidden-xs">
                        <div class="image-box">
                            <img class="img-responsive" src="<?php echo base_url(); ?>assets/img/slider/anak.jpg" alt="">
                        </div>
                    </div><!-- /col-md-2 -->

                    <div class="col-xs-12 col-sm-5 col-md-5">
                        <div class="right-column">
                            <div class="wow zoomIn media">
                                <div class="media-left media-middle">
                                    <a href="#">
                                        <i class="fa flaticon-leaf32"></i>
                                    </a>
                                </div><!-- media left-->
                                <div class="media-body">
                                    <h2>DPT</h2>
                                    <h3>Mencegah difteri, batuk rejan </h3>
                                    <span>dan Tetanus.</span>
                                </div><!--/.media body-->
                            </div><!-- /.media -->
                            <div class="wow zoomIn media">
                                <div class="media-left media-middle">
                                    <a href="#">
                                        <i class="fa fa-ambulance"></i>
                                    </a>
                                </div><!-- media left-->
                                <div class="media-body">
                                    <h2>Campak</h2>
                                    <h3>Mencegah campak (radang paru, radang otak</h3>
                                    <span>dan kebutaan)</span>
                                </div><!--/.media body-->
                            </div><!-- /.media -->
                            <div class="wow zoomIn media">
                                <div class="media-left media-middle">
                                    <a href="#">
                                        <i class="fa fa-question"></i>
                                    </a>
                                </div><!-- media left-->
                                <div class="media-body">
                                    <h2>Jadi?</h2>
                                    <h3>Imunisasi melindungi anak dari penyakit, mencegah anak cacat </h3>
                                    <span>dan kematian anak.</span>
                                </div><!--/.media body-->
                            </div><!-- /.media -->
                        </div><!-- /.right-column -->
                    </div>
                </div><!-- row-->
            </div><!-- container -->
        </section><!-- /our services -->
        <!-- CLEAN IDEA AND UNIQUE DESIGN -->
        <section id="clean" class="clean-idea section-padding">
            <div class="container">
                <div class="row">
                    <div class="wow zoomIn col-xs-12 text-center p-padding">
                        <h1 class="section-title">clean idea and unique design</h1>
                        <p>A designer knows he has achieved perfection not when there is nothingleft to add, but when there is nothing left to take away.</p>
                        <div class="button-set">
                            <button type="button" class="btn btn-danger"><i class="fa fa-link"></i>Read More</button>
                            <a href="#contact" class="page-scroll contact-us"><i class="fa fa-phone"></i>Contact Us</a>
                        </div><!-- /.button-set -->
                    </div><!-- col-xs-12 -->
                </div><!-- row-->
            </div><!-- container -->
        </section><!-- /clean idea and unique design -->
        <!-- OUR LATEST WORKS -->
        <section class="protfolio section-padding" id="product">
            <div class="container">
                <div class="row">
                    <div class="wow zoomIn col-xs-12 text-center p-padding animated" style="visibility: visible; animation-name: zoomIn;">
                        <h1 class="section-title">pastikan buah hati anda sehat</h1>
                        <p>Anak bukan tamu biasa di rumah kita. Mereka telah dipinjamkan untuk sementara waktu kepada kita dengan tujuan mencintai mereka dan menanamkan nilai-nilai dasar untuk kehidupan masa depan yang akan mereka bangun – Dr. James C Dobson.</p>
                    </div>
                </div><!-- row -->
            </div><!-- container -->
            <div class="portfolio-menu">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <ul class="wow zoomIn animated" id="filter" style="visibility: visible; animation-name: zoomIn;">
                                <li><a data-group="all" href="#" class="active">imunisasi</a></li>
                                <li><a data-group="wordpress" href="#">wajib asi</a></li>
                                <li><a data-group="video" href="#">sediakan obat</a></li>
                                <li><a data-group="image" href="#">pastikan bersih</a></li>
                                <li><a data-group="branding" href="#">selalu cek pertumbuhanya</a></li>
                            </ul><!-- /#filter -->
                        </div><!-- /.col-xs-12 -->
                    </div>
                </div>
            </div>



            <div class="row">
                <div id="grid" class="shuffle" style="position: relative; height: 600px;">
                    <!-- portfolio-item -->
                    <div data-groups='["all", "image", "video"]' class=" portfolio-item col-xs-12 col-sm-6 col-md-3 shuffle-item filtered animated" style="margin:0; padding:0;">
                        <div class="portfolio">
                            <figure class="effect-julia">
                                <img alt="img21" src="<?php echo base_url(); ?>assets/img/portfolio01.jpg">
                                <figcaption>
                                    <div class="socials">
                                        <a data-toggle="modal" data-target="#myModal1" href=""><i class="fa fa-expand"></i></a>
                                        <a href=""><i class="fa fa-share animated"></i></a>
                                    </div>
                                    <div class="scoial-heading">
                                        <p>Mornign Dew</p>
                                        <strong>Icons, Illustrations</strong>
                                    </div>
                                </figcaption>
                            </figure>
                        </div><!-- /.portfolio -->
                    </div><!-- /.portfolio-item -->
                    <!-- MODAL/POPUP -->
                    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class=" btn btn-default pull-right" data-dismiss="modal">X</button>
                                    <span class="modal-title" id="myModalLabel">Our recent work</span>
                                </div>
                                <div class="modal-body">
                                    <img src="<?php echo base_url(); ?>assets/img/portfolio01.jpg" alt="img21" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- MODAL/POPUP -->
                    <!-- portfolio-item -->
                    <div data-groups='["all", "image","wordpress","branding"]' class=" portfolio-item col-xs-12 col-sm-6 col-md-3 shuffle-item filtered " style="margin:0; padding:0;">
                        <div class="portfolio">
                            <figure class="effect-julia">
                                <img alt="img21" src="<?php echo base_url(); ?>assets/img/portfolio02.jpg">
                                <figcaption>
                                    <div class="socials">
                                        <a data-toggle="modal" data-target="#myModal2" href=""><i class="fa fa-expand"></i></a>
                                        <a href=""><i class="fa fa-share animated"></i></a>
                                    </div>
                                    <div class="scoial-heading">
                                        <p>Mornign Dew</p>
                                        <strong>Icons, Illustrations</strong>
                                    </div>
                                </figcaption>
                            </figure>
                        </div><!-- /.portfolio -->
                    </div><!-- /portfolio-item -->
                    <!-- MODAL/POPUP -->
                    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class=" btn btn-default pull-right" data-dismiss="modal">X</button>
                                    <span class="modal-title" id="myModalLabel">Our recent work</span>
                                </div>
                                <div class="modal-body">
                                    <img src="<?php echo base_url(); ?>assets/img/portfolio02.jpg" alt="img21" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- MODAL/POPUP -->
                    <!-- portfolio-item -->
                    <div data-groups='["all", "image","wordpress", "circle", "video"]' class=" portfolio-item col-xs-12 col-sm-6 col-md-3 shuffle-item filtered " style="margin:0; padding:0;">
                        <div class="portfolio">
                            <figure class="effect-julia">
                                <img alt="img21" src="<?php echo base_url(); ?>assets/img/portfolio03.jpg">
                                <figcaption>
                                    <div class="socials">
                                        <a data-toggle="modal" data-target="#myModal3" href=""><i class="fa fa-expand"></i></a>
                                        <a href=""><i class="fa fa-share animated"></i></a>
                                    </div>
                                    <div class="scoial-heading">
                                        <p>Mornign Dew</p>
                                        <strong>Icons, Illustrations</strong>
                                    </div>
                                </figcaption>
                            </figure>
                        </div><!-- /.portfolio -->
                    </div><!-- /portfolio-item -->
                    <!-- MODAL/POPUP -->
                    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class=" btn btn-default pull-right" data-dismiss="modal">X</button>
                                    <span class="modal-title" id="myModalLabel">Our recent work</span>
                                </div>
                                <div class="modal-body">
                                    <img src="<?php echo base_url(); ?>assets/img/portfolio03.jpg" alt="img21" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- MODAL/POPUP -->
                    <!-- portfolio-item -->
                    <div data-groups='["image", "all","wordpress", "branding"]' class=" portfolio-item col-xs-12 col-sm-6 col-md-3 shuffle-item filtered " style="margin:0; padding:0;">
                        <div class="portfolio">
                            <figure class="effect-julia">
                                <img alt="img21" src="<?php echo base_url(); ?>assets/img/portfolio04.jpg">
                                <figcaption>
                                    <div class="socials">
                                        <a data-toggle="modal" data-target="#myModal4" href=""><i class="fa fa-expand"></i></a>
                                        <a href=""><i class="fa fa-share animated"></i></a>
                                    </div>
                                    <div class="scoial-heading">
                                        <p>Mornign Dew</p>
                                        <strong>Icons, Illustrations</strong>
                                    </div>
                                </figcaption>
                            </figure>
                        </div><!-- /.portfolio -->
                    </div><!-- /portfolio-item -->
                    <!-- MODAL/POPUP -->
                    <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class=" btn btn-default pull-right" data-dismiss="modal">X</button>
                                    <span class="modal-title" id="myModalLabel">Our recent work</span>
                                </div>
                                <div class="modal-body">
                                    <img src="<?php echo base_url(); ?>assets/img/portfolio04.jpg" alt="img21" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- MODAL/POPUP -->
                    <!-- portfolio-item -->
                    <div data-groups='["all","wordpress", "video", "branding"]' class=" portfolio-item col-xs-12 col-sm-6 col-md-3 shuffle-item filtered " style="margin:0; padding:0;">
                        <div class="portfolio">
                            <figure class="effect-julia">
                                <img alt="img21" src="<?php echo base_url(); ?>assets/img/portfolio05.jpg">
                                <figcaption>
                                    <div class="socials">
                                        <a data-toggle="modal" data-target="#myModal5" href=""><i class="fa fa-expand"></i></a>
                                        <a href=""><i class="fa fa-share animated"></i></a>
                                    </div>
                                    <div class="scoial-heading">
                                        <p>Mornign Dew</p>
                                        <strong>Icons, Illustrations</strong>
                                    </div>
                                </figcaption>
                            </figure>
                        </div><!-- /.portfolio -->
                    </div><!-- /portfolio-item -->
                    <!-- MODAL/POPUP -->
                    <div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class=" btn btn-default pull-right" data-dismiss="modal">X</button>
                                    <span class="modal-title" id="myModalLabel">Our recent work</span>
                                </div>
                                <div class="modal-body">
                                    <img src="<?php echo base_url(); ?>assets/img/portfolio05.jpg" alt="img21" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- MODAL/POPUP -->
                    <!-- portfolio-item -->
                    <div data-groups='["image", "all","wprdpress", "branding", "video"]' class=" portfolio-item col-xs-12 col-sm-6 col-md-3 shuffle-item filtered " style="margin:0; padding:0;">
                        <div class="portfolio">
                            <figure class="effect-julia">
                                <img alt="img21" src="<?php echo base_url(); ?>assets/img/portfolio06.jpg">
                                <figcaption>
                                    <div class="socials">
                                        <a data-toggle="modal" data-target="#myModal6" href=""><i class="fa fa-expand"></i></a>
                                        <a href=""><i class="fa fa-share animated"></i></a>
                                    </div>
                                    <div class="scoial-heading">
                                        <p>Mornign Dew</p>
                                        <strong>Icons, Illustrations</strong>
                                    </div>
                                </figcaption>
                            </figure>
                        </div><!-- /.portfolio -->
                    </div><!-- /portfolio-item -->
                    <!-- MODAL/POPUP -->
                    <div class="modal fade" id="myModal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class=" btn btn-default pull-right" data-dismiss="modal">X</button>
                                    <span class="modal-title" id="myModalLabel">Our recent work</span>
                                </div>
                                <div class="modal-body">
                                    <img src="<?php echo base_url(); ?>assets/img/portfolio06.jpg" alt="img21" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- MODAL/POPUP -->
                    <!-- portfolio-item -->
                    <div data-groups='["all", "branding", "video"]' class=" portfolio-item col-xs-12 col-sm-6 col-md-3 shuffle-item filtered " style="margin:0; padding:0;">
                        <div class="portfolio">
                            <figure class="effect-julia">
                                <img alt="img21" src="<?php echo base_url(); ?>assets/img/portfolio07.jpg">
                                <figcaption>
                                    <div class="socials">
                                        <a data-toggle="modal" data-target="#myModal7" href=""><i class="fa fa-expand"></i></a>
                                        <a href=""><i class="fa fa-share animated"></i></a>
                                    </div>
                                    <div class="scoial-heading">
                                        <p>Mornign Dew</p>
                                        <strong>Icons, Illustrations</strong>
                                    </div>
                                </figcaption>
                            </figure>
                        </div><!-- /.portfolio -->
                    </div><!-- /portfolio-item -->
                    <!-- MODAL/POPUP -->
                    <div class="modal fade" id="myModal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class=" btn btn-default pull-right" data-dismiss="modal">X</button>
                                    <span class="modal-title" id="myModalLabel">Our recent work</span>
                                </div>
                                <div class="modal-body">
                                    <img src="<?php echo base_url(); ?>assets/img/portfolio07.jpg" alt="img21" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- MODAL/POPUP -->
                    <!-- portfolio-item -->
                    <div data-groups='["image", "all", "wordpress", "video"]' class=" portfolio-item col-xs-12 col-sm-6 col-md-3 shuffle-item filtered " style="margin:0; padding:0;">
                        <div class="portfolio">
                            <figure class="effect-julia">
                                <img alt="img21" src="<?php echo base_url(); ?>assets/img/portfolio08.jpg">
                                <figcaption>
                                    <div class="socials">
                                        <a data-toggle="modal" data-target="#myModal8" href=""><i class="fa fa-expand"></i></a>
                                        <a href=""><i class="fa fa-share animated"></i></a>
                                    </div>
                                    <div class="scoial-heading">
                                        <p>Mornign Dew</p>
                                        <strong>Icons, Illustrations</strong>
                                    </div>
                                </figcaption>
                            </figure>
                        </div><!-- /.portfolio -->
                    </div><!-- /portfolio-item -->
                    <!-- MODAL/POPUP -->
                    <div class="modal fade" id="myModal8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">X</button>
                                    <span class="modal-title" id="myModalLabel">Our recent work</span>
                                </div>
                                <div class="modal-body">
                                    <img src="<?php echo base_url(); ?>assets/img/portfolio08.jpg" alt="img21" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- MODAL/POPUP -->
                </div> <!-- /grid -->
            </div><!-- /row -->
        </section><!-- /our latest works -->

        <div class="more-area">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <h2 class="pull-left">Ingin bergabung dengan ribuan ibu dan bidan lainnya? Register sekarang</h2>
                        <a href="<?php echo base_url(); ?>daftar"><button type="button" id="load" class="btn btn-success pull-right">Daftar</button></a>
                    </div>
                </div><!-- row -->
            </div><!-- container -->
        </div><!-- /.more-area -->
        <!-- COUNTING -->
        <section class="counting section-padding">
            <div class="container">
                <div class="row" style="margin-left:350px;">
                
                    <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                        <span class="counter count3">5065</span>
                        <p>pasien</p>
                    </div><!-- /.col-sm-3 -->
                    <div class="col-xs-12 col-sm-6 col-md-3 text-center">
                        <span class="counter count4">2154</span>
                        <p>bidan</p>
                    </div><!-- /.col-sm-3 -->

                </div> <!-- /.row -->
            </div><!-- /.container -->
        </section><!-- counting -->
        
        <!-- BLOG CONTENT -->
        <section id="blog" class="blog-wrapper section-padding">
            <div class="container">
                <div class="row">
                    <div class="wow zoomIn col-xs-12 text-center p-padding">
                        <h1 class="section-title">Info</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed accumsan venenatis lectus sed sollicitudin. Duis in odio ex. Sed elementum varius enim. In vel tincidunt lorem. Donec gravida felis vitae ipsum pharetra maximus.</p>
                    </div><!-- col-xs-12 -->
                    <div class="wow zoomIn col-xs-12 col-md-4">
                        <img class="img-responsive" src="<?php echo base_url(); ?>assets/img/blog1.jpg" alt="Image" />
                        <div class="blog-date-wrapper">
                            <span class="floatleft"><i class="fa fa-calendar"></i> 01th june 2015</span>
                            <span class="floatright"><i class="fa fa-thumbs-o-up"></i> 30</span>
                            <span class="floatright"><i class="fa fa-comments"></i> 25 </span>
                            <span class="clearboth"> &nbsp; </span>
                        </div>
                        <div class="blog-container">
                            <h2><a href="blog-sidebar.html" target="_blank">Tips Merawat Bayi</a></h2>
                            <p>Berapa kali sehari, sih, bayi harus dimandikan? Kenapa pula bokongnya kerap berwarna merah? Perlukah kita membersihkan lidah si kecil? </p>
                            <a class="custom-button" href="blog-sidebar.html">read more <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <div class="wow zoomIn col-xs-12 col-md-4">
                        <img class="img-responsive" src="<?php echo base_url(); ?>assets/img/blog2.jpg" alt="Image" />
                        <div class="blog-date-wrapper">
                            <span class="floatleft"><i class="fa fa-calendar"></i> 01th june 2015</span>
                            <span class="floatright"><i class="fa fa-thumbs-o-up"></i> 30</span>
                            <span class="floatright"><i class="fa fa-comments"></i> 25 </span>
                            <span class="clearboth"> &nbsp; </span>
                        </div>
                        <div class="blog-container">
                            <h2><a href="blog-sidebar.html" target="_blank"> Tips Obat-obatan yang harus ada di rumah</a></h2>
                            <p>Penyakit langganan yang sering dijumpai pada anak-anak dan balita antara lain; demam, diare, batuk, pilek, dan muntah. Selain itu.</p>
                            <a class="custom-button" href="blog-sidebar.html">read more <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                    <div class="wow zoomIn col-xs-12 col-md-4">
                        <img class="img-responsive" src="<?php echo base_url(); ?>assets/img/blog3.jpg" alt="Image" />
                        <div class="blog-date-wrapper">
                            <span class="floatleft"><i class="fa fa-calendar"></i> 01th june 2015</span>
                            <span class="floatright"><i class="fa fa-thumbs-o-up"></i> 30</span>
                            <span class="floatright"><i class="fa fa-comments"></i> 25 </span>
                            <span class="clearboth"> &nbsp; </span>
                        </div>
                        <div class="blog-container">
                            <h2><a href="blog-sidebar.html" target="_blank">Strategi Memberi Makanan Sehat Pada Anak</a></h2>
                            <p>Pamerkan kebiasaan sehat Anda. Anak adalah peniru ulung. Makanya, Anda (plus suami) harus kompak dan membuat si kecil selalu.</p>
                            <a class="custom-button" href="blog-sidebar.html">read more <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.container -->
            <div class="more-area">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="pull-left">anaksehatku</h2>
                            <button type="button" class="btn btn-success pull-right">see More</button>
                        </div><!-- col-xs-12 -->
                    </div><!-- row -->
                </div><!-- container -->
            </div><!-- /.more-area -->
        </section><!-- blog content -->
        
        <!-- footer -->
        <footer>
            <div class="footer-wrapper section-padding">
                <div class="container">
                    <div class="row">
                        <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3">
                            <a href="index.html"><img src="<?php echo base_url(); ?>assets/img/footer-logo.png" alt="AnakSehatku"></a>
                            <p class="footer-content">Aplikasi gratis ditunjukkan untuk kemudahan Ibu dan Bidan.</p>
                        </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                         <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3">
                            <p class="footer-heading">find us</p>
                            <ul class="footercontact">
                                <li><i class="flaticon-mainpage"></i><span>address:</span> One TECHGUT loop, 54100</li>
                                <li><i class="flaticon-phone16"></i><span>phone:</span><a href="tel:88 02 8714612"> +88 02 8714612</a></li>
                                <li><i class="flaticon-email21"></i><span>e-mail:</span><a href="mailto:support@themerole.com"> support@themerole.com</a></li>
                                <li><i class="flaticon-world91"></i><span>web:</span><a href="http://themerole.com"> www.themerole.com</a></li>
                            </ul>
                            <i class="flaticon-home78"></i>
                        </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                        <div class="wow zoomIn col-xs-12 col-sm-6 col-md-3">
                            <p class="footer-heading">recent posts</p>
                            <ul class="footerblog">
                                <li><a href="blog-sidebar.html">The Green Fields of Spring</a> <p>13th Jun 2014</p></li>
                                <li><a href="blog-sidebar.html">This is a Video Post</a> <p>18th Nov 2014</p></li>
                                <li><a href="blog-sidebar.html">Satisfaction Lies in the Effort</a> <p>13th Jun 2014</p></li>
                            </ul>
                        </div><!-- /.col-xs-12 .col-sm-3 .col-md-3 -->
                    </div> <!-- /.row -->
                </div> <!-- /.container -->
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="wow zoomIn col-xs-12">
                            <p>© 2015 All rights reserved. <span>anaksehatku</span> theme by <a href="http://themerole.com">themerole</a></p>
                            <div class="backtop  pull-right">
                                <i class="fa fa-angle-up back-to-top"></i>
                            </div><!-- /.backtop -->
                        </div><!-- /.col-xs-12 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.creditwrapper -->
        </footer><!-- /Footer -->
        <!-- MMENU -->
        <nav id="menu">
            <ul>
                <li><a href="#">home</a></li>
                <li><a href="#about">about us</a></li>
                <li><a href="#services">services</a></li>
                <li><a href="#product">product</a></li>
                <li>
                    <a href="#blog">blog</a>
                    <ul>
                        <li><a href="blog-full-width.html">full width blog</a></li>
                        <li><a href="blog-sidebar.html">sidebar blog</a></li>
                    </ul>
                </li>
                <li><a href="#contact">contact</a></li>
            </ul>
        </nav><!-- /#menu -->

    </div><!-- /.main -->
    <!-- jQuery JS -->
    <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.js"></script>

    <!-- Modernizr JS -->
    <script src="<?php echo base_url(); ?>assets/js/modernizr-2.6.2.min.js"></script>

    <!--Bootatrap JS-->
    <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- REVOLUTION Slider  -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/rs-plugin/js/jquery.themepunch.revolution.js"></script>

    <!-- Shuffle JS -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.shuffle.min.js"></script>

    <!-- mmenu -->
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/vendor/mmenu/js/jquery.mmenu.min.js"></script>

    <!-- Owl Carosel -->
    <script src="<?php echo base_url(); ?>assets/vendor/owl/js/owl.carousel.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>

    <!-- waypoints JS-->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script>

    <!-- Counterup JS -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.counterup.min.js"></script>

    <!-- Easing JS -->
    <script src="<?php echo base_url(); ?>assets/js/jquery.easing.min.js"></script>

    <!-- Smooth Scroll JS -->
    <script src="<?php echo base_url(); ?>assets/js/scrolling-nav.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/smoothscroll.min.js"></script>

    <!-- Custom Script JS -->
    <script src="<?php echo base_url(); ?>assets/js/script.js"></script>

    <!-- Email JS -->
    <script src="<?php echo base_url(); ?>assets/js/email.js"></script>

    <script>
$(window).on('scroll', function(){
  if( $(window).scrollTop()>670 ){
    $('.navbar-default').addClass('navbar-fixed-top');
  } else {
    $('.navbar-default').removeClass('navbar-fixed-top');
  }
});
    </script>

</body>
</html>
