<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bidan extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('m_bidan');
        $this->load->model('m_broadcast');
   		$this->load->helper(array('url')); //load helper url
    }

    public function message(){
		$noTujuan = "081315230386";//$_POST['nohp'];
		$message = $this->input->post('message');;//$_POST['msg'];
		$result = $this->m_broadcast->broadcast();

		foreach($result as $row){
			$cmd = 'start cmd.exe @cmd /k "c:\gammu\bin\gammu.exe -c c:\gammu\bin\smsdrc sendsms TEXT '.$row->nomor_telepon.' -text "'.$message.'"';
			pclose(popen("start /B ". $cmd, "r"));
		}
	}
	

	public function index(){
		$this->load->view('bidan');
	}

	public function catatankesehatan(){
		$this->load->view('catatankesehatan');
	}

	public function vitamin(){
		$this->load->view('vitamin');
	}

	public function deteksidini(){
		$this->load->view('deteksidini');
	}

	public function lingkarkepala(){
		$this->load->view('lingkarkepala');
	}	
	
	public function beratbadan(){
		$this->load->view('timbangan');
	}
	public function rekammedis(){
		$this->load->view('catatanpenyakit');
	}
	public function broadcast(){
		$this->load->view('broadcast');
	}

	public function s_catatankesehatan(){
		$berat = $this->input->post('berat');
		$tinggi = $this->input->post('tinggi');
		$suhu = $this->input->post('suhu');
		$keluhan = $this->input->post('keluhan');
		$penyakit = $this->input->post('penyakit');
		$napas = $this->input->post('napas');
		$denyut = $this->input->post('denyut');
		$diare = $this->input->post('diare');
		$ikterus = $this->input->post('ikterus');
		$kemungkinanbb = $this->input->post('kemungkinanbb');
		$vitamink = $this->input->post('vitamink');
		$status = $this->input->post('status');
		$keluhanibu = $this->input->post('keluhanibu');
		$keluhanlain = $this->input->post('keluhanlain');
		$kunjungan = $this->input->post('kunjungan');
		$tanggal = $this->input->post('tanggal');
		$pasien = $this->input->post('pasien');
		$bidan = $this->input->post('bidan');


		$data = array(
			'berat' => $berat,
			'tinggi' => $tinggi,
			'suhu' => $suhu,
			'keluhan' => $keluhan,
			'penyakit' => $penyakit,
			'napas' => $napas,
			'denyut' => $denyut,
			'diare' => $diare,
			'ikterus' => $ikterus,
			'kemungkinanbb' => $kemungkinanbb,
			'vitamink1' => $vitamink,
			'status' => $status,
			'keluhanlain' => $keluhanlain,
			'keluhanibu' => $keluhanibu,
			'kunjungan' => $kunjungan,
			'tanggal_kunjungan' => $tanggal,
			'pasien' => $pasien,
			'bidan' => $bidan
		);
		$this->m_bidan->insert_catatankesehatan($data);
		redirect('bidan');

	}

	public function s_vitamin(){
		$tangal = $this->input->post('tanggal');
		$umur = $this->input->post('umur');
		$pasien = $this->input->post('pasien');
		$bidan = $this->input->post('bidan');
		
		$data = array(
			'umur' => $umur,
			'tanggal' => $tangal,
			'pasien' => $pasien,
			'bidan' => $bidan
			
		);
		$this->m_bidan->insert_vitamin($data);
		redirect('bidan');

	}

	public function s_deteksidini(){
		$umuranjuran = $this->input->post('umuranjuran');
		$tanggalanjuran = $this->input->post('tanggalanjuran');
		$umurnasehat = $this->input->post('umurnasehat');
		$tanggalnasehat = $this->input->post('tanggalnasehat');
		$pasien = $this->input->post('pasien');
		$bidan = $this->input->post('bidan');
		
		$data = array(
			'umuranjuran' => $umuranjuran,
			'tanggalanjuran' => $tanggalanjuran,
			'tanggalnasihat' => $tanggalnasehat,
			'umurnasihat' => $umurnasehat,
			'pasien' => $pasien,
			'bidan' => $bidan
			
		);
		$this->m_bidan->insert_deteksidini($data);
		redirect('bidan');

	}

	public function s_lingkarkepala(){
		$umur = $this->input->post('umur');
		$lingkarkepala = $this->input->post('lingkarkepala');
		$tanggal = $this->input->post('tanggal');
		$pasien = $this->input->post('pasien');
		$bidan = $this->input->post('bidan');
		
		$data = array(
			'umur' => $umur,
			'lingkar' => $lingkarkepala,
			'tanggal' => $tanggal,
			'pasien' => $pasien,
			'bidan' => $bidan
			
		);
		$this->m_bidan->insert_lingkarkepala($data);
		redirect('bidan');

	}

	public function s_beratbadan(){
		$beratbadan = $this->input->post('beratbadan');
		$umur = $this->input->post('umur');
		$bulantimbang = $this->input->post('bulantimbang');
		$pasien = $this->input->post('pasien');
		$bidan = $this->input->post('bidan');
		
		$data = array(
			'beratbadan' => $beratbadan,
			'umur' => $umur,
			'bulantimbang' => $bulantimbang,
			'pasien' => $pasien,
			'bidan' => $bidan
			
		);
		$this->m_bidan->insert_beratbadan($data);
		redirect('bidan');

	}

	public function s_catatanpenyakit(){
		$tanggal = $this->input->post('tanggal');
		$penyakit = $this->input->post('penyakit');
		$tindakan = $this->input->post('tindakan');
		$keterangan = $this->input->post('keterangan');
		$pasien = $this->input->post('pasien');
		$bidan = $this->input->post('bidan');
		
		$data = array(
			'tanggal' => $tanggal,
			'penyakit' => $penyakit,
			'tindakan' => $tindakan,
			'keterangan' => $keterangan,
			'pasien' => $pasien,
			'bidan' => $bidan
			
		);
		$this->m_bidan->insert_catatanpenyakit($data);
		redirect('bidan');

	}
}