<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Daftar extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->model('m_daftar');
   		$this->load->helper(array('url')); //load helper url
    }

	public function index(){
		$this->load->view('daftar');
	}

	public function pasien(){
		$this->load->view('daftarpasien');
	}

	public function bidan(){
		$this->load->view('daftarbidan');
	}

	public function daftarbidan(){
		$nama = $this->input->post('nama');
		$jk = $this->input->post('jeniskelamin');
		$no_regis = $this->input->post('no_regis');
		$nik = $this->input->post('nik');
		$instansi = $this->input->post('instansi');
		$alamat = $this->input->post('alamat');
		$kodepos = $this->input->post('kodepos');
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$data = array(
			'nik' => $nik,
			'nama' => $nama,
			'alamat' => $alamat,
			'instansi' => $instansi,
			'jeniskelamin' => $jk,
			'no_registrasi_bidan' => $no_regis,
			'username' => $username,
			'password' => $password
		);
		$this->m_daftar->insert_bidan($data);
		redirect('login');

	}

	public function daftarpasien(){
		$namaanak = $this->input->post('namaanak');
		$jk = $this->input->post('jeniskelamin');
		$tmpt_anak = $this->input->post('tempatanak');
		$tgl_anak = $this->input->post('tanggalanak');
		$umur = $this->input->post('umur');
		$nama_ibu = $this->input->post('namaibu');
		$nama_suami = $this->input->post('suami');
		$tempatibu = $this->input->post('tempatibu');
		$tanggalibu = $this->input->post('tanggalibu');
		$golongandarah = $this->input->post('golongandarah');
		$telp = $this->input->post('telp');
		$pendidikanibu = $this->input->post('pendidikanibu');
		$pekerjaanibu = $this->input->post('pekerjaanibu');
		$pendidikansuami = $this->input->post('pendidikansuami');
		$pekerjaansuami = $this->input->post('pekerjaansuami');
		$alamat = $this->input->post('alamat');
		$kodepos = $this->input->post('kodepos');
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$data = array(
			'nama_anak' => $namaanak,
			'jk' => $jk,
			'tempat_anak' => $tmpt_anak,
			'tanggal_anak' => $tgl_anak,
			'umur' => $umur,
			'nama_ibu' => $nama_ibu,
			'nama_suami' => $nama_suami,
			'tempat_ibu' => $tempatibu,
			'tanggal_ibu' => $tanggalibu,
			'gol_darah' => $golongandarah,
			'telp' => $telp,
			'pendidikan_ibu' => $pendidikanibu,
			'pekerjaan_ibu' => $pekerjaanibu,
			'pendidikan_suami' => $pendidikansuami,
			'pekerjaan_suami' => $pekerjaansuami,
			'alamat' => $alamat,
			'kodepos' => $kodepos,
			'username' => $username,
			'password' => $password
		);
		$this->m_daftar->insert_pasien($data);
		redirect('login');

	}
}