<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller {
	$this->session->unset_userdata('logged_in');
	redirect('welcome');
}