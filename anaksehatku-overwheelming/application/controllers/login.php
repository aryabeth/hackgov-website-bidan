<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('m_login');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function proseslogin(){
		$username = $this->input->post('username');
		$password = $this->input->post('password');
   		$result = $this->m_login->loginPasien($username, $password);
   		$result2 = $this->m_login->loginBidan($username, $password);
   		if($result)
   		{
	    $sess_array = array();
	    foreach($result as $row)
		    {
		    $sess_array = array(
		        'id' => $row->id,
		        'username' => $row->username
		    );
       	$this->session->set_userdata('logged_in', $sess_array);
     	}
	       	$this->session->set_userdata('logged_in', $sess_array);
	       	redirect('pasien');
       	}else{
	       	if($result2)
	   		{
		    $sess_array = array();
		    foreach($result2 as $row)
			    {
			    $sess_array = array(
			        'id' => $row->id,
			        'username' => $row->username
			    );
	       	$this->session->set_userdata('logged_in', $sess_array);
	     	}
		       	$this->session->set_userdata('logged_in', $sess_array);
		       	redirect('bidan');
	       	}
       		$this->load->view('login');
       	}
	}

	public function logout() 
	{
		$this->session->unset_userdata('logged_in');
		redirect('welcome');
		
	}

}

