<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pasien extends CI_Controller {
	public function index(){
		$this->gcharts->load('LineChart');
		$this->gcharts->DataTable('Stocks')
		              ->addColumn('number', 'normal', 'normal')
		              ->addColumn('number', 'Batas normal', 'normal')
		              ->addColumn('number', 'Batas normal', 'kelebihan');

		for($a = 1; $a < 24; $a++)
		{
			$temp = $a;
			if($a>=4)$temp+=1;
			if($a>6)$temp-=0.05*$a;
			if($a>7)$temp-=0.05*$a;
			if($a>8)$temp-=0.05*$a;
			if($a>9)$temp-=0.05*$a;
			if($a>10)$temp-=0.05*$a;
			if($a>11)$temp-=0.05*$a;
			if($a>12)$temp-=0.05*$a;
			if($a>13)$temp-=0.05*$a;
			if($a>14)$temp-=0.04*$a;
			if($a>15)$temp-=0.04*$a;
			if($a>16)$temp-=0.03*$a;
			if($a>17)$temp-=0.03*$a;
			if($a>18)$temp-=0.02*$a;
			if($a>19)$temp-=0.02*$a;
			if($a>20)$temp-=0.02*$a;
			if($a>21)$temp-=0.02*$a;
			if($a>22)$temp-=0.02*$a;
			if($a>23)$temp-=0.02*$a;
		    $data = array(
		        $a,             //Count
		        $temp+$a/4, //Line 1's data
		        $temp  //Line 2's data
		    );
		    $this->gcharts->DataTable('Stocks')->addRow($data);
		}

		$config = array(
		    'title' => 'Stocks'
		);

		$this->gcharts->LineChart('Stocks')->setConfig($config);
		$this->load->view('pasien');
	}
}