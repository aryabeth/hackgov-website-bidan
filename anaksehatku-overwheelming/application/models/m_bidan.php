<?php
Class m_bidan extends CI_Model
{
	
	function insert_catatankesehatan($data){
     	$this->db->insert('pemeriksaan', $data);
     	return TRUE;
  }

  function insert_vitamin($data){
      $this->db->insert('vitamin', $data);
      return TRUE;
  }

  function insert_deteksidini($data){
      $this->db->insert('deteksidini', $data);
      return TRUE;
  }  

  function insert_lingkarkepala($data){
      $this->db->insert('lingkarkepala', $data);
      return TRUE;
  }  

  function insert_beratbadan($data){
      $this->db->insert('timbangan', $data);
      return TRUE;
  } 

  function insert_catatanpenyakit($data){
      $this->db->insert('rekammedis', $data);
      return TRUE;
  }  
}
?>