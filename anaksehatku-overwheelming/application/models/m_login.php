<?php
class m_login extends CI_Controller {
	function loginPasien($username, $password)
	{
	  $this -> db -> select('id, username, password');
	  $this -> db -> from('pasien');
	  $this -> db -> where('username', $username);
	  $this -> db -> where('password', $password);
	  $this -> db -> limit(1);
	
	  $query = $this -> db -> get();
	
	  if($query -> num_rows() == 1)
	  {
	    return $query->result();
	  }
	  else
	  {
	    return false;
	  }
	}
	function loginBidan($username, $password){
		$this -> db -> select('id, username, password');
		$this -> db -> from('bidan');
		$this -> db -> where('username', $username);
		$this -> db -> where('password', $password);
		$this -> db -> limit(1);
	  	$query = $this -> db -> get();
	
	  	if($query -> num_rows() == 1)
	  	{
	    	return $query->result();
	  	}
	  	else
	  	{
	   		return false;
	  	}
	}
}
?>