<?php 
class m_daftar extends CI_Model{
	function __construct()
    {
      parent::__construct();
    }

	function insert_bidan($data){
     	$this->db->insert('bidan', $data);
     	return TRUE;
  }

  function insert_pasien($data){
  	$this->db->insert('pasien', $data);
     	return TRUE;	
  }
}